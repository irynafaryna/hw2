const express = require('express');

const router = express.Router();

const { getProfileInfo, changePassword, deleteProfile } = require('./usersService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.get('/', authMiddleware, getProfileInfo);

router.patch('/', authMiddleware, changePassword);

router.delete('/', authMiddleware, deleteProfile);

module.exports = {
  usersRouter: router,
};
