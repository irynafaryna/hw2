// const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://Iryna:newpassword@cluster0.c1cziwy.mongodb.net/homework2?retryWrites=true&w=majority');

const { authRouter } = require('./authRouter');
const { notesRouter } = require('./notesRouter');
const { usersRouter } = require('./usersRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/notes', notesRouter);
app.use('/api/users/me', usersRouter);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
function errorHandler(err, req, res) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}

app.use(errorHandler);
