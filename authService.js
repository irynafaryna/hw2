const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('./models/Users');

const registerUser = async (req, res) => {
  try {
    const { username, password } = req.body;

    if (!username || !password || typeof username !== 'string' || typeof password !== 'string') {
      res.status(400).send({ message: 'bad request' });
    }
    const user = new User({
      username,
      password: await bcrypt.hash(password, 10),
    });
    await user.save();
    res.status(200).send({ message: 'Success' });
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const loginUser = async (req, res) => {
  try {
    const user = await User.findOne({ username: req.body.username });
    if (!req.body.username || !req.body.password || typeof req.body.password !== 'string' || typeof req.body.username !== 'string') {
      res.status(400).json({ message: 'Bad request' });
    }
    if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
      // eslint-disable-next-line
      const payload = { username: user.username, userId: user._id };
      const jwtToken = jwt.sign(payload, 'secret-jwt-key');
      return res.status(200).json({
        message: 'Success',
        jwt_token: jwtToken,
      });
    }
    return res.status(400).json({ message: 'Not authorized' });
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

module.exports = {
  registerUser,
  loginUser,
};
