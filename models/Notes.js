const mongoose = require('mongoose');

const Note = mongoose.model('note', {
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    format: Date,
    default: new Date().toISOString(),
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
});

module.exports = {
  Note,
};
