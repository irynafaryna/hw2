const mongoose = require('mongoose');

const User = mongoose.model('user', {
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    format: Date,
    default: new Date().toISOString(),
  },
});

module.exports = {
  User,
};
