const { Note } = require('./models/Notes');

async function createNote(req, res) {
  try {
    const { text } = req.body;
    const { userId } = req.user;
    if (!text || !userId || typeof text !== 'string' || typeof userId !== 'string') {
      res.status(400).json({ message: 'Bad request' });
    }
    const note = new Note({
      text,
      userId,
    });
    return await note.save().then(() => {
      res.status(200).json({
        message: 'Success',
      });
    });
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
}

async function getNotes(req, res) {
  try {
    const { offset, limit } = req.query;
    const { userId } = req.user;
    const allUserNotes = await Note.find({
      userId,
    });
    const filterResult = await Note.find({
      userId,
    }).skip(+offset)
      .limit(+limit);
    return res.status(200).send({
      offset,
      limit,
      count: allUserNotes.length,
      notes: filterResult,
    });
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
}

async function getNote(req, res) {
  try {
    if (req.params.id) {
      return await Note.findById(req.params.id)
        .then((note) => {
          res.status(200).json({
            note,
          });
        });
    } else {
     res.status(400).json({ message: 'Bad request' });
    }
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
}

const updateNote = async (req, res) => {
  try {
    const { text } = req.body;
    const note = await Note.findOneAndUpdate({_id: req.params.id }, { text }).then(() => res.status(200).json({ message: 'Success' }));
    if (!note || !text) {
      res.status(400).json({ message: 'Bad request' });
    }
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const checkNote = async (req, res) => {
  try {
    const note = await Note.findOne({ _id: req.params.id });
    await Note.findOneAndUpdate({ _id: req.params.id }, { completed: !note.completed })
      .then(() => res.status(200).json({ message: 'Success' }));
    if (!note) {
      res.status(400).json({ message: 'Bad request' });
    }
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

const deleteNote = async (req, res) => {
  try {
    if (req.params.id) {
      await Note.findByIdAndDelete(req.params.id)
        .then(() => {
          res.status(200).json({ message: 'Success' });
        });
    } else {
      res.status(400).json({ message: 'Bad request' });
    }
  } catch (err) {
    return res.status(500).send({ message: 'Server error' });
  }
};

module.exports = {
  createNote,
  getNotes,
  getNote,
  updateNote,
  checkNote,
  deleteNote,
};
